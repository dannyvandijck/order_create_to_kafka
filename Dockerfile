FROM openjdk:8-jre-slim
MAINTAINER Danny Van Dijck <danny.vandijck@hotmail.com>
ADD target/ordercreate.jar ordercreate.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar", "/ordercreate.jar"]
EXPOSE 1234
