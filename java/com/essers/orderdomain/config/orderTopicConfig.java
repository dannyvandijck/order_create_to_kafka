package com.essers.orderdomain.config;

import org.springframework.cloud.stream.annotation.EnableBinding;

import com.essers.orderdomain.stream.OrderTopicStreams;

@EnableBinding(OrderTopicStreams.class)
public class orderTopicConfig {

}
