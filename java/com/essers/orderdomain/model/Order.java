package com.essers.orderdomain.model;

import java.util.Date;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class Order {
	private long id;
	private double orderTotal;
	private long orderCustomerId;
	private Date orderCreationTimestamp;

}
