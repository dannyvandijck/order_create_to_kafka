package com.essers.orderdomain.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface OrderTopicStreams {

	String CREATE_NEW_ORDER_PUT_ON_STREAM = "orderTopic-new";

	@Input(CREATE_NEW_ORDER_PUT_ON_STREAM)
	SubscribableChannel readUpdateForOrderFromOrderTopic();

	@Output(CREATE_NEW_ORDER_PUT_ON_STREAM)
	MessageChannel writeNewOrderToOrderTopic();

}
