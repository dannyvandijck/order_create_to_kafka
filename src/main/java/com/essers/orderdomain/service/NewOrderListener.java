package com.essers.orderdomain.service;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.essers.orderdomain.model.Order;
import com.essers.orderdomain.stream.OrderTopicStreams;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class NewOrderListener {
	@StreamListener(OrderTopicStreams.CREATE_NEW_ORDER_PUT_ON_STREAM)
	public void readOrderFromTopic(@Payload Order order) {
		log.info("Received order: {}", order);
	}
}
