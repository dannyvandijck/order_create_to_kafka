package com.essers.orderdomain.service;

import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

import com.essers.orderdomain.model.Order;
import com.essers.orderdomain.stream.OrderTopicStreams;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OrderService {

	private final OrderTopicStreams orderTopicStreams;

	public OrderService(OrderTopicStreams orderTopicStreams) {
		this.orderTopicStreams = orderTopicStreams;
	}

	public void sendCreatedOrderToTopic(final Order order) {
		log.info("Sending order {}", order);
		MessageChannel messageChannel = orderTopicStreams.writeNewOrderToOrderTopic();
		messageChannel.send(MessageBuilder.withPayload(order)
				.setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
				.build());
	}
}
