package com.essers.orderdomain.web;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.essers.orderdomain.model.Order;
import com.essers.orderdomain.service.OrderService;

@RestController
public class OrderController {
	private final OrderService orderService;

	public OrderController(OrderService orderService) {
		this.orderService = orderService;
	}

	@GetMapping("/order")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void createNewOrder(@RequestParam("orderId") long orderId,
			@RequestParam("orderCustomerName") long orderCustomerName, @RequestParam("orderTotal") double orderTotal) {
		Order newOrderRecieved = Order.builder()
				.id(orderId)
				.orderCustomerId(orderCustomerName)
				.orderTotal(orderTotal)
				.orderCreationTimestamp(new Date())
				.build();
		orderService.sendCreatedOrderToTopic(newOrderRecieved);
	}

}
